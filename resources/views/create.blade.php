<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    
    <title>Tambah Buku</title>
  </head>
  <body style="background: #f1f1f1">
    <div class="container nt-5">
      <div class="row">
        <div class="col-md-12">
          <div class="card border-0 shadow rounded">

            <div class="card-header">
              <h4 class="text-center">
                <i class="bi bi-book"></i> Tambah Buku
              </h4>
            </div>

            <div class="card-body">
                <form action="{{ route('buku.store') }}" method="post" enctype="multipart/form-data" autocomplete="off">
                    @csrf

                    <div class="form-group">
                        <label for="Judul" class="fw-bold"></label>
                        <input type="text" class="form-control" placeholder="Judul" value="{{ old('Judul')}}" name="judul">
                        @error('Judul')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror 
                    </div>

                    <div class="form-group">
                        <label for="Penulis" class="fw-bold"></label>
                        <input type="text" class="form-control" placeholder="Penulis" value="{{ old('Penulis')}}" name="penulis">
                        @error('Penulis')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror 
                    </div>

                    <div class="form-group">
                        <label for="Tahun" class="fw-bold">Tahun</label>
                        <input type="text" class="form-control" placeholder="Tahun" value="{{ old('Tahun')}}" name="tahun_terbit">
                        @error('Tahun_Terbit')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror 
                    </div>

                    <div class="from-danger mt-2">
                      <div style="widht:200px">
                        <img src ="{{ Storage::url('public/buku/default.jpg') }}" class="img-fluid img-thumbnail" id="preview">
                      </div>
                    </div>

                    <div class="form-group">
                        <label for="cover" class="fw-fold">Cover</label>
                        <input type="file" name="cover" class="form-control">
                        @error('cover')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror 
                    </div>

                    <div class="form-group d-flex justify-content-end mt-3">
                      <a href="{{ route('buku.index') }}" class="btn btn-outline-secondary mx-2">Batal</a>
                      <button type="submit" class="btn btn-primary"><i class="bi bi-sd-card"></i>Simpan</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $('input[type=file]').on('change', function(e) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $('#preview').attr('src', e.target.result);
          }
          reader.readAsDataURL(this.files[0]);
        });
    </script>
  </body>
</html>